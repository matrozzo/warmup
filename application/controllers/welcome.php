<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -  
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     * 
     * model:
     * 
     * $this->welcome_model->get_all();
     * $this->welcome_model->get_by(array data);
     * $this->welcome_model->insert(array data);
     * $this->welcome_model->update(array data, array where);
     * $this->welcome_model->delete(array data);
     * $this->welcome_model->get_nearest_address(float lat, float long, int distance);
     * 
     */
    
    public function __construct()
	{
	    parent::__construct();
        $this->load->model('welcome_model');
	}
        
	public function index()
	{
        $data['title']      = 'New Address';
        $data['address']    = null;
        $data['action']     = 'new';
        
		$this->load->view('welcome_message', $data);
	}
    
    public function show_list()
	{
		$data['address'] = $this->welcome_model->get_all();
		$this->load->view('show_list', $data);
	}
	
	public function search_distance()
	{
		$data['address'] = $this->welcome_model->get_all();
		$this->load->view('search_distance', $data);
	}
    
    public function search_nearest()
    {
        $post   = $this->input->post();
        $json   = array('status' => 1,'message' => 'success', 'data' => $post);
        
        $errors = $this->validateSearchData($post);
        
        if(count($errors) > 0) {
            // Set error
            $json = array(
                'status'  => 0,
                'message' => 'error',
                'data'    => $post,
                'errors'  => $errors
            );
            
        } else {
            // Search 
            $lat    = $post['latitude'];
            $long   = $post['longitude'];
            $distance = $post['distance'];
            $results = $this->welcome_model->get_nearest_address($lat, $long, $distance);
            
            $json['data'] = $results;
        }
        
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }
       
    public function show()
    {
        $data['points'] = $this->welcome_model->get_all();
        //die(var_dump($data['points']));
        $this->load->view('show_view', $data);
    }
    
    public function get_all()
    {
        $json = $this->welcome_model->get_all();
        echo json_encode($json);
    }
        
    public function save_address()
    {
        $post   = $this->input->post();
        $json   = array('status' => 1,'message' => 'success', 'data' => $post);
        
        $errors = $this->validateData($post);
        
        if(count($errors) > 0) {
            // Set error
            $json = array(
                'status'  => 0,
                'message' => 'error',
                'data'    => $post,
                'errors'  => $errors
            );
            
        } else {
            // TODO: Error control
            // Save address
            $data = $post;
            unset($data['action'], $data['id']);
            
            if($post['action'] == 'new')
                $this->welcome_model->insert($data);
            else
                $this->welcome_model->update($data, array('id' => $post['id']));
        }
        
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }
    
    public function delete_address($id)
    {
        $json   = array('status' => 1,'message' => 'success');
        $data = array('id' => $id);
        // Check if exist
        $address = $this->welcome_model->get_by($data);
        if(empty($address))
        {
            // Set error
            $json = array(
                'status'  => 0,
                'message' => 'error',
                'error_msj'  => 'This address not exist'
            );
        }
        else
        {
            try 
            {
                // Delete
                $this->welcome_model->delete($data);
            } 
            catch (Exception $exc) 
            {
                $json = array(
                    'status'  => 0,
                    'message' => 'error',
                    'error_msj'  => 'Error in DB. Please, reload the page and try again later.'
                );
                
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($json));
            }
        }
        
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }
    
    public function edit_address($id)
    {
        // TODO: Add error control for bad id
        // Get Address
        $address = $this->welcome_model->get_by(array('id' => $id));
        
        $data['title'] = 'Edit '.$address[0]->address_1;
        $data['address'] = $address[0];
        $data['action'] = 'edit';
        $this->load->view('welcome_message', $data);
    }
    
    private function validateData($data)
    {
        $errors = array();
        $strExpReg = '/^[\pL\d]{1}[\pL\'-\s\d#]*$/iu';
        $zipcodeExpReg = '/^[0-9]{5}$/';
        
        // Validate Address
        if(!preg_match($strExpReg, $data['address_1'])) {
            $errors['address_1'] = 'You must enter a valid Address';
        }
        
        // Validate City
        if(!preg_match($strExpReg, $data['city'])) {
            $errors['city'] = 'You must enter a valid City';
        }
        
        // Validate State
        if(!preg_match($strExpReg, $data['state'])) {
            $errors['state'] = 'You must enter a valid State';
        }
        
        // Validate Zipcode
        if(!preg_match($zipcodeExpReg, $data['zipcode'])) {
            $errors['zipcode'] = 'You must enter a valid Zipcode';
        }
        
        // Validate Latitude
        if(!filter_var($data['latitude'], FILTER_VALIDATE_FLOAT)) {
            $errors['latitude'] = 'You must enter a valid Latitude';
        }
        
        // Validate Longitude
        if(!filter_var($data['longitude'], FILTER_VALIDATE_FLOAT)) {
            $errors['longitude'] = 'You must enter a valid Longitude';
        }
        
        return $errors;
    }
    
    private function validateSearchData($data)
    {
        $errors = array();
        $strExpReg = '/^[\pL\d]{1}[\pL\'-\s\d#,]*$/iu';
        
        // Validate Address
        if(!preg_match($strExpReg, $data['address_1'])) {
            $errors['address_1'] = 'You must enter a valid Address';
        }
        
        // Validate Latitude
        if(!filter_var($data['latitude'], FILTER_VALIDATE_FLOAT)) {
            $errors['latitude'] = 'You must enter a valid Latitude';
        }
        
        // Validate Longitude
        if(!filter_var($data['longitude'], FILTER_VALIDATE_FLOAT)) {
            $errors['longitude'] = 'You must enter a valid Longitude';
        }
        
        // Validate Longitude
        if(!filter_var($data['distance'], FILTER_VALIDATE_INT)) {
            $errors['distance'] = 'You must enter a valid distance to search';
        }
        
        return $errors;
    }
    
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */