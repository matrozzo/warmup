<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Welcome_model extends CI_Model {

        public function get_all()
        {
            return $this->db->select()
                    ->from('location')
                    ->get()
                    ->result();
        }
        
        /**
         * 
         * @param array $where
         * @return array
         */
        function get_by($where)
        {
            return $this->db->get_where('location', $where)
                    ->result();
        }
        
        /**
         * 
         * @param array $data
         * @return array with status and id
         */
        function insert($data)
        {
            $return['status'] = $this->db->insert('location', $data);
            $return['id'] = $this->db->insert_id();
            return $return;
        }
        
        /**
         * 
         * @param array $data
         * @param array $where
         * @return array
         */
        function update($data, $where)
        {
            $return['status'] = $this->db->update('location', $data, $where);
            return $return;
        }
        
        /**
         * delete value in db
         * @param array $data
         * @param string $table
         * @return boolean
         */
        function delete($data)
        {
            return $this->db->delete('location', $data);
        }
        
        /**
         * delete value in db
         * @param array $data
         * @param string $table
         * @return boolean
         */
        function get_nearest_address ($lat, $long, $distance)
        {
            $sql = "CALL filterByDistance(?, ?, ?)";
            $parameters = array($lat, $long, $distance);
            $query = $this->db->query($sql, $parameters);

            return $query->result();
        }
}