<!DOCTYPE html>
<html>
    <head>
        <title>Address List | Trulia</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/public/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="/public/css/styles.css" />
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,200" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!-- Header -->
        <header>
            <div id="header-wrapper">
                <img src="/public/img/logo.png" title="Trulia" alt="Trulia"/>
                <div class="title">Trulia</div>
                <div class="sub-title">"The Trulia test web page."</div>
            </div>
        </header>
        <!-- End Header -->
        
        <!-- Menu -->
        <nav>
            <ul id="nav-wrapper">
                <li><a href="/">Home</a></li>
                <li class="active"><a href="#">Address List</a></li>
				<li><a href="/search-distance">Search</a></li>
            </ul>
        </nav>
        <!-- End Menu -->
        
        <!-- Content -->
        <div id="content-wrapper">
            <h1 class="title">Address List</h1>
            
            <a href="/address">Add New Address &#187;</a>
            
            <table>
                <thead>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Zipcode</th>
                    <th>Coordinates</th>
                    <th style="width: 170px">Actions</th>
                </thead>
                <tbody>
                    <?php if(isset($address) && count($address) > 0 ): ?>
                        <?php foreach($address as $item): ?>
                        <tr data-id="<?php echo $item->id ?>">
                            <td><?php echo $item->address_1 ?></td>
                            <td><?php echo $item->city ?></td>
                            <td><?php echo $item->state ?></td>
                            <td><?php echo $item->zipcode ?></td>
                            <td><?php echo 'Lat: '.$item->latitude.'<br />Long: '.$item->longitude ?></td>
                            <td>
                                <ul class="actions">
                                    <li><button type="button" name="delete" class="delete">Delete</button></li>
                                    <li><button type="button" name="edit" class="edit">Edit</button></li>
                                </ul>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr><td colspan="10" style="text-align: center;font-weight: bold;">No Address to show</td></tr>
                    <?php endif; ?>
                </tbody>
            </table>
            
        </div>
        <!-- End Content -->
        
        <!-- Footer -->
        <footer>
            <div id="footer-wrapper">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="#">Address List</a></li>
                </ul>
                <span class="copyright">&#169; Copyright 2013</span>
            </div>
        </footer>
        <!-- End Footer -->
        
        <!-- Include JS -->
        <script type="text/javascript" src="/public/js/lib/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="/public/js/address-list.js"></script>
    </body>
</html>