<!DOCTYPE html>
<html>
    <head>
        <title>Trulia</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/public/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="/public/css/styles.css" />
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,200" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!-- Header -->
        <header>
            <div id="header-wrapper">
                <img src="/public/img/logo.png" title="Trulia" alt="Trulia"/>
                <div class="title">Trulia</div>
                <div class="sub-title">"The Trulia test web page."</div>
            </div>
        </header>
        <!-- End Header -->
        
        <!-- Menu -->
        <nav>
            <ul id="nav-wrapper">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="/address-list">Address List</a></li>
				<li><a href="/search-distance">Search</a></li>
            </ul>
        </nav>
        <!-- End Menu -->
        
        <!-- Content -->
        <div id="content-wrapper">
            <h1 class="title">Map</h1>
            
            <div id="map"></div>
            <div id="list">
                <ul>
                    <?php foreach ($points as $key => $point){?>
                        <li>
                            <a href="#" onclick="set_position(<?php echo $point->latitude;?>,<?php echo $point->longitude;?>, 16); return false;"> 
                                <?php echo $point->address_1;?>
                            </a>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
        
        <!-- End Content -->
        
        <!-- Footer -->
        <footer>
            <div id="footer-wrapper">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="/address-list">Address List</a></li>
                </ul>
                <span class="copyright">&#169; Copyright 2013</span>
            </div>
        </footer>
        <!-- End Footer -->
        
        <!-- Include JS -->
        <script type="text/javascript" src="/public/js/lib/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="/public/js/maps.show.js"></script>
    </body>
</html>