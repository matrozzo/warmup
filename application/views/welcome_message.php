<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title ?> | Trulia</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/public/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="/public/css/styles.css" />
        <link rel="stylesheet" type="text/css" href="/public/css/validationEngine.jquery.css" />
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,200" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!-- Header -->
        <header>
            <div id="header-wrapper">
                <img src="/public/img/logo.png" title="Trulia" alt="Trulia"/>
                <div class="title">Trulia</div>
                <div class="sub-title">"The Trulia test web page."</div>
            </div>
        </header>
        <!-- End Header -->
        
        <!-- Menu -->
        <nav>
            <ul id="nav-wrapper">
                <li><a href="/">Home</a></li>
                <li class="active"><a href="/address-list">Address List</a></li>
				<li><a href="/search-distance">Search</a></li>
            </ul>
        </nav>
        <!-- End Menu -->
        
        <!-- Content -->
        <div id="content-wrapper">
            <h1 class="title"><?php echo $title ?></h1>
            
            <div id="add-address-form-wrapper">
                <form action="#" method="post" id="add-address-form">
                    <fieldset>
                        <!-- Hide inputs -->
                        <input type="hidden" name="id" id="id" value="<?php echo ($address)? $address->id : '' ?>" />
                        <input type="hidden" name="action" id="action" value="<?php echo $action ?>" />
                        <!-- End Hide inputs -->

                        <div class="item">
                            <label for="address_1">Address</label>
                            <input type="text" class="address_1 validate[required]" name="address_1" id="address_1" value="<?php echo ($address)? $address->address_1 : '' ?>" placeholder="Enter The Address" required/>
                        </div>
                        
                        <div class="item">
                            <label for="city">City</label>
                            <input type="text" class="city validate[required]" name="city" id="city" value="<?php echo ($address)? $address->city : '' ?>" placeholder="Enter The City Name" required/>
                        </div>
                        
                        <div class="item">
                            <label for="state">State</label>
                            <input type="text" class="state validate[required]" name="state" id="state" value="<?php echo ($address)? $address->state : '' ?>" placeholder="Enter The State" required/>
                        </div>
                        
                        <div class="item">
                            <label for="zipcode">Zipcode</label>
                            <input type="text" class="zipcode validate[required]" name="zipcode" id="zipcode" value="<?php echo ($address)? $address->zipcode : '' ?>" placeholder="Enter The Zipcode" required/>
                        </div>

                        <div class="item">
                            <label for="latitude">Latitude</label>
                            <input type="text" class="latitude" name="latitude" id="latitude" value="<?php echo ($address)? $address->latitude : '' ?>" placeholder="Latitude" required readonly/>
                        </div>
                        
                        <div class="item">
                            <label for="longitude">Longitude</label>
                            <input type="text" class="longitude" name="longitude" id="longitude" value="<?php echo ($address)? $address->longitude : '' ?>" placeholder="Longitude" required readonly/>
                        </div>
                        
                        <br />
                        <button type="submit" name="save" id="save" value="Save">Save</button>
                        <button type="reset" name="reset" id="reset" value="reset">Reset</button>
                        <button type="button" name="cancel" id="cancel" value="Cancel">Cancel</button>
                    </fieldset>
                </form>
                
                <div id="map-wrapper"></div>
                
            </div>
            
        </div>
        
        <!-- End Content -->
        
        <!-- Footer -->
        <footer>
            <div id="footer-wrapper">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/address-list">Address List</a></li>
                </ul>
                <span class="copyright">&#169; Copyright 2013</span>
            </div>
        </footer>
        <!-- End Footer -->
        
        <!-- Include JS -->
        <script type="text/javascript" src="/public/js/lib/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="/public/js/lib/gmaps.js"></script>
        <script type="text/javascript" src="/public/js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="/public/js/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="/public/js/map.js"></script>
    </body>
</html>