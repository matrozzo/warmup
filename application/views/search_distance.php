<!DOCTYPE html>
<html>
    <head>
        <title>Search Addresses | Trulia</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/public/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="/public/css/styles.css" />
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,200" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!-- Header -->
        <header>
            <div id="header-wrapper">
                <img src="/public/img/logo.png" title="Trulia" alt="Trulia"/>
                <div class="title">Trulia</div>
                <div class="sub-title">"The Trulia test web page."</div>
            </div>
        </header>
        <!-- End Header -->
        
        <!-- Menu -->
        <nav>
            <ul id="nav-wrapper">
                <li><a href="/">Home</a></li>
                <li><a href="/address-list">Address List</a></li>
				<li class="active"><a href="#">Search</a></li>
            </ul>
        </nav>
        <!-- End Menu -->
        
        <!-- Content -->
        <div id="content-wrapper">
            <h1 class="title">Search Addresses</h1>
            
			<div id="add-address-form-wrapper">
                <form action="#" method="post" id="search-distance-form">
                    <fieldset>
                        <!-- Hide inputs -->
                        <input type="hidden" name="latitude" id="latitude" />
                        <input type="hidden" name="longitude" id="longitude" />
                        <!-- End Hide inputs -->
                        
                        <div class="item">
                            <label for="address_1">Address</label>
                            <input type="text" class="address_1" name="address_1" id="address_1" value="" placeholder="Enter The Address" required/>
                        </div>
                        
                        <div class="item">
                            <label for="distance">Distance in miles</label>
                            <input type="text" class="distance" name="distance" id="distance" value="" placeholder="Enter The Distance" required/>
                        </div>
                        
                        <br />
                        <button type="submit" name="search" id="search" value="Search">Search</button>
                        <button type="reset" name="reset" id="reset" value="Reset">Reset</button>
                        <button type="button" name="clear" id="clear" value="clear">Clear</button>
                    </fieldset>
                </form>
                
                <div id="map-wrapper"></div>
                
            </div>
		</div>
        <!-- End Content -->
        
        <!-- Footer -->
        <footer>
            <div id="footer-wrapper">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="#">Address List</a></li>
                </ul>
                <span class="copyright">&#169; Copyright 2013</span>
            </div>
        </footer>
        <!-- End Footer -->
        
        <!-- Include JS -->
        <script type="text/javascript" src="/public/js/lib/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="/public/js/lib/gmaps.js"></script>
        <script type="text/javascript" src="/public/js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="/public/js/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="/public/js/map.js"></script>
    </body>
</html>