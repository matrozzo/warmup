DROP PROCEDURE IF EXISTS trulia_dev.filterByDistance;

DELIMITER ;;
CREATE PROCEDURE filterByDistance(IN mylat DOUBLE,IN mylon DOUBLE,IN dist INT)

BEGIN
    DECLARE lon1 FLOAT; 
    DECLARE lon2 FLOAT;
    DECLARE lat1 FLOAT; 
    DECLARE lat2 FLOAT;

    -- These variables help to rule out results that will be too far away from the destination with a simple calculation
    -- This makes the store procedure to run faster on large data sets.
    SET lon1 = mylon-dist/ABS(COS(RADIANS(mylat))*69);
    SET lon2 = mylon+dist/ABS(COS(RADIANS(mylat))*69);
    SET lat1 = mylat-(dist/69); 
    SET lat2 = mylat+(dist/69);

    SELECT 
        *, 3956 * 2 * ASIN( SQRT( 
            POWER(SIN((mylat - latitude) * PI()/180 / 2), 2) + COS(mylat * PI()/180) * COS(latitude * PI()/180) *POWER(SIN((mylon - longitude) * PI()/180 / 2), 2)
        )) AS distance 
    FROM 
        location
    WHERE 
        longitude BETWEEN lon1 AND lon2 AND latitude BETWEEN lat1 AND lat2 
    HAVING 
        distance <= dist ORDER BY distance LIMIT 10;
END;;
DELIMITER ;
