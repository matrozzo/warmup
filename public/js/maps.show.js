//Blobal 

defaul_lat = 34.042629;
defaul_lng = -118.464951;
defaul_zoon = 14;

var infowindow = null;

$(document).ready(function () { 
    initialize();  
});

function initialize() {

    var centerMap = new google.maps.LatLng(defaul_lat, defaul_lng);

    var myOptions = {
        zoom: defaul_zoon,
        center: centerMap
    };

    var map = new google.maps.Map(document.getElementById("map"), myOptions);

    setMarkers(map);
    infowindow = new google.maps.InfoWindow({
        content: "loading..."
    });

    var bikeLayer = new google.maps.BicyclingLayer();
    bikeLayer.setMap(map);
}

function setMarkers(map) {
    var url= "/welcome/get_all";
    $.ajax({
        url: url,
        Type:"GET",
        cache: false,
        dataType: "json",
        success: function(json) {
                for (var i = 0; i < json.length; i++) {
                    var sites = json[i];
                    var siteLatLng = new google.maps.LatLng(sites['latitude'], sites['longitude']);
                    var marker = new google.maps.Marker({
                        position: siteLatLng,
                        map: map,
                        title: sites['address_1'],
                        zIndex: 3,
                        html: sites['address_1']
                    });

                     google.maps.event.addListener(marker, "click", function () {
                        infowindow.setContent(this.html);
                        infowindow.open(map, this);
                    });

                }
        }
    });
}

function set_position(lat, lng, zoon) {
    defaul_lat = lat;
    defaul_lng = lng;
    defaul_zoon = zoon
    initialize();
}




