jQuery(document).ready(function($) {
    jQuery("#add-address-form").validationEngine();
});

var app = app || {};

app.Map = {
    container: '#map-wrapper',
    map: new GMaps({
        div: '#map-wrapper',
        lat: 34.042629,
        lng: -118.464951
    }),
            
    /**
     * Init Function
     * @param options
     */
    init: function initFn(options) {
        var _this = this;
        _this.container = (options && options.container) ? options.container : _this.container;
        // Attach Events
        _this.attachEvents();
    },
    /**
     * Attach Events
     */
    attachEvents: function attachEventsFn() {
        var _this = this;
        
        // Init edit
        if($('#action').val() === 'edit') {
            var lat = $('#latitude').val(),
                lng = $('#longitude').val();
        
            _this.map.setCenter(lat, lng);
            _this.map.addMarker({lat: lat, lng: lng});
        }

        // Remove errors from inputs (if has)
        $('input').on('focus', function(e) {
            e.preventDefault();

            var elem = $(e.currentTarget),
                message = elem.next('.error-message');

            elem.removeClass('error');
            message.remove();
        });

        var callbackFn = function(results, status) {
            if (status == 'OK') {
                var latlng = results[0].geometry.location;
                
                //This prevents that two diferent "search" actions show the results at the same time
                //the correct way to handle this is to make sure that only the last search result 
                //gets rendered (will work on that tomorrow)
                _this.map.removeMarkers(); 
                _this.map.setCenter(latlng.lat(), latlng.lng());
                _this.map.addMarker({
                    lat: latlng.lat(),
                    lng: latlng.lng()
                });
                // Update latitud and longitude fields
                $('#latitude').val(latlng.lat());
                $('#longitude').val(latlng.lng());
            }
        };
                
        var searchFn = function(e) {
            GMaps.geocode({
                address: _this.buildFullAddress(),
                callback: callbackFn
            });
        };
                
        // Search action (search and blur)
        $('#address_1').on('blur', searchFn);
        $('#city').on('blur', searchFn);
        $('#state').on('blur', searchFn);
        $('#zipcode').on('blur', searchFn);

        // Cancel New Address Button
        $('#cancel').on('click', function(e) {
            e.preventDefault();
            // TODO: Cancel action
            
            // $('#add-address-form-wrapper').slideUp();
            $.each($('#add-address-form').find('input'), function(key, value) {
                $(value).val('');
            });
        });

        // Save Address Button
        $('#save').on('click', function(e) {
            e.preventDefault();
            
            // Remove old errors
            $('input').removeClass('error');
            $('.error-message').remove();
            
            // TODO: add validator
            var form = $('#add-address-form');
            
            $.ajax({
                type: "POST",
                url: "/address/save",
                data: form.serialize()
            })
            .done(function(data) 
            {
                if(data.status === 1) {
                    _this.renderMessage('success', 'The address <strong>'+data.data.address_1+'</strong> is saved successfully');
                } else {
                    $.each(data.errors, function(key, value){
                        $('.'+key)
                            .addClass('error')
                            .after('<span class="error-message">'+value+'</span>');
                    });
                }
                
            });

        });
        
        // Cancel Button
        $('#cancel').on('click', function(e){
            e.preventDefault();
            window.location.href = '/address-list';
        });
        
        // Search Button (Search Address)
        $('#search').on('click', function(e){
            e.preventDefault();
            var form = $('#search-distance-form');
            
            // Remove old errors
            $('input').removeClass('error');
            $('.error-message').remove();
            
            $.ajax({
                type: "POST",
                url: "/search-distance/search",
                data: form.serialize()
            })
            .done(function(data) 
            {
                $('.search-results').remove();
                
                if(data.status === 1) {
                    //_this.renderMessage('success', 'The address <strong>'+data.data.address_1+'</strong> is saved successfully');
                    var div = $('<ul class="search-results"></ul>');
                    $.each(data.data, function(key, value){
                        div.append($('<li><a href="#" onclick="return false;">'+value.address_1+'</li>'));
                    });
                    
                    form.after(div);
                    
                } else {
                    $.each(data.errors, function(key, value){
                        $('.'+key)
                            .addClass('error')
                            .after('<span class="error-message">'+value+'</span>');
                    });
                }
                
            });
        });
        
    },
    
    /**
    * Render Message function
    * @param status
    * @param message
    */
    renderMessage: function renderMessageFn (status, message) {
        var _this = this,
                messageWrapper = $('<div>').addClass('message ' + status).html(message);
        $('h1.title').after(messageWrapper);
        setTimeout(_this.removeMessage, 3500);
    },
            
    /**
     * Remove Message function
     */
    removeMessage: function removeMessageFn () {
        $('.message').fadeOut('slow');
    },
    
    /**
     * Retrieves the full address from the form
     */
    buildFullAddress: function buildFullAddressFn() {
        var address = $('#address_1').val();
        
        if ($('#city').val() !== '') {
            address += ', ' + $('#city').val();
        }
        
        if ($('#state').val() !== '') {
            address += ', ' + $('#state').val();
        }
        
        if ($('#zipcode').val() !== '') {
            address += ' ' + $('#zipcode').val();
        }
        
        return address;
    }

};

app.Map.init();