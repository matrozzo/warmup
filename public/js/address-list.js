var app = app || {};

app.AddressList = {
                
    /**
     * Init Function
     * @param options
     */
    init: function initFn(options) {
        var _this = this;
        // Attach Events
        _this.attachEvents();
    },
    /**
     * Attach Events
     */
    attachEvents: function attachEventsFn() {
        var _this = this;

        // Delete Address
        $('table').on('click','.delete', function(e) {
            e.preventDefault();
            var target = $(e.currentTarget),
                row = target.closest('tr'),
                id = row.data('id');

            if(!confirm('Are you sure to delete this Address?'))
                return false;
            
            // TODO: Add more security to this call (session or token)
            $.ajax({
                type: "GET",
                url: "address/delete/"+id
            })
            .done(function(data) 
            {
                if(data.status === 1) {
                    _this.renderMessage('info', 'The address <strong>'+$.text(row.children()[0])+'</strong> is remove successfully');
                    row.remove();
                } else {
                    _this.renderMessage('error', data.error_msj);
                }
            });
            
        });

        // Edit Contact
        $('table').on('click','.edit', function(e) {
            e.preventDefault();
            var id = $(e.currentTarget).closest('tr').data('id');
            window.location.href = '/address/edit/'+id;
        });
    },
            
    /**
    * Render Message function
    * @param status
    * @param message
    */
    renderMessage: function renderMessageFn (status, message) {
        var _this = this,
                messageWrapper = $('<div>').addClass('message ' + status).html(message);
        $('h1.title').after(messageWrapper);
        setTimeout(_this.removeMessage, 3500);
    },
            
    /**
     * Remove Message function
     */
    removeMessage: function removeMessageFn () {
        $('.message').fadeOut('slow');
    }

};

app.AddressList.init();